package com.nesterov;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        // Создание экземпляра класса HashSet для хранения уникальных элементов.
        Set<String> stringSet = new HashSet<>();
        // Добавление элементов.
        stringSet.add("Apple");
        stringSet.add("Banana");
        stringSet.add("Orange");
        stringSet.add("Apple"); // Повторяющийся элемент не бедет добавлен.

        System.out.println("Размер множества: " + stringSet.size());

        // Проверка наличия элемента.
        if (stringSet.contains("Banana")) {
            System.out.println("Множество содержит 'Banana'");
        }

        // Удаление элемента.
        stringSet.remove("Orange");

        // Вывод всех элементов.
        for (String fruit : stringSet) {
            System.out.println(fruit);
        }

        // Очистка.
        stringSet.clear();
    }
}